<?php

require_once('./methods.php');

unset($_SESSION['UserID']);
unset($_SESSION['Latitude']);
unset($_SESSION['Longitude']);
unset($_SESSION['isTechnician']);

header('Location: http://yare.us/wow');

?>