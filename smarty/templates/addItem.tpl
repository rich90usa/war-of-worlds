{extends file='layout.tpl'}

{block name=pageTitle}Create Item{/block}

{block name=pageContent}
<form action="api.php" method="get">
<input type="hidden" name="method" value="addItem">
<strong>Name:</strong><br /> <input type="text" name="name" required><br>
<strong>Description:</strong><br /> <input type="text" name="description" required><br>
<strong>Vendor Name:</strong><br /> <input type="text" name="vendorname" required><br>
<strong>Scenario:</strong><br /> <input type="text" name="scenario" list="scenarios" required><br>
<datalist id="scenarios">
   <option value="Morning">
   <option value="Day">
   <option value="Night">
</datalist>
<strong>Class:</strong><br /> <input type="text" name="class" list="itemClasses" required><br>
<datalist id="itemClasses">
   <option value="SMG">
   <option value="Pistol">
   <option value="Shotgun">
   <option value="Eridian">
   <option value="Drink">
   <option value="Sniper Rifle">
   <option value="Shield">
</datalist>
<strong>Cost:</strong><br /> <input type="text" name="cost" required><br>
<input class="btn" type="submit" value="Create">
</form>
{/block}