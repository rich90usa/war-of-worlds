{extends file='layout.tpl'}

{block name=pageTitle}Setting Location{/block}

{block name=pageContent}
Asking for permission...
<script type="text/javascript">

function loadScript() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        alert("It doesn't look like this is going to work :(");
        window.location.href = 'http://yare.us/wow/';
    }
}

function showPosition(position) {
    console.warn(position);
    window.location.href = 'http://yare.us/wow/api.php?method=setLocation&latitude=' + position.coords.latitude + '&longitude=' + position.coords.longitude;
}

window.onload = loadScript;
</script>
{/block}