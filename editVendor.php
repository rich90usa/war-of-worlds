<?php

require_once('./smarty/smarty_main.inc');
require_once('./methods.php');

$db = getDB();

if (isset($_GET['vendorID'])) {
    $vendorID = $_GET['vendorID'];

    $vendor = getVendorByID($vendorID);
    $vendor = $vendor['result'][0];

    $smarty->assign('vendor', $vendor);
    $smarty->assign('vendorID', $vendorID);
}

$smarty->display('extends:layout.tpl|editVendor.tpl');

closeDB($db);

?>