{extends file='layout.tpl'}

{block name=pageTitle}
{if isset($smarty.session.UserID)}
Welcome Back Agent {$smarty.session.UserID}
{else}
Access Denied
{/if}
{/block}


{block name=pageContent}
{if isset($smarty.session.Latitude) and isset($smarty.session.Longitude)}
<div id="map-canvas" style="height:300px"></div><br />
{/if}
{if isset($smarty.session.UserID)}

{else}
This device is restricted to authorized agents.<br />
{/if}
{if isset($smarty.session.Latitude) and isset($smarty.session.Longitude)}
<script type="text/javascript">
function initialize() {
  var mapOptions = {
    zoom: 13,
    center: new google.maps.LatLng({$smarty.session.Latitude}, {$smarty.session.Longitude}),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

    var userMarker = 
        new google.maps.Marker({
            map:map,
            icon: {
              path: google.maps.SymbolPath.CIRCLE,
              fillColor: 'red',
              strokeColor: 'white',
              fillOpacity: 1.0,
              strokeOpacity: 1.0,
              strokeWeight: 4.0,
              scale: 5
            },
            draggable:false,
            position: new google.maps.LatLng({$smarty.session.Latitude}, {$smarty.session.Longitude})
        });
}

function loadScript() {
  var script = document.createElement("script");
  script.type = "text/javascript";
  script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyCv2K7qIWGVpygfRaf9rqspLFR-fSfnlzU&sensor=true&callback=initialize";
  document.body.appendChild(script);
}


window.onload = loadScript;
</script>
{/if}
{/block}