<?php

require_once('./smarty/smarty_main.inc');
require_once('./methods.php');

$db = getDB();

$allItems = getAllItems();
$allItems = $allItems['result'];

$smarty->assign('inventory', $allItems);

$smarty->display('extends:layout.tpl|inventory.tpl');

closeDB($db);

?>