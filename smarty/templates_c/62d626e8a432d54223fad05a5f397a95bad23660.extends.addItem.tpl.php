<?php /* Smarty version Smarty-3.1.13, created on 2013-03-18 01:16:21
         compiled from "/var/www/sites/yare/wow/smarty/templates/addItem.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2861588745144ca0ce1f8d3-01246704%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '62d626e8a432d54223fad05a5f397a95bad23660' => 
    array (
      0 => '/var/www/sites/yare/wow/smarty/templates/addItem.tpl',
      1 => 1363554980,
      2 => 'file',
    ),
    'fc05a559fcc48881c073fded6a8797d44a7c3a5f' => 
    array (
      0 => '/var/www/sites/yare/wow/smarty/templates/layout.tpl',
      1 => 1363554592,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2861588745144ca0ce1f8d3-01246704',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5144ca0ceba9c7_18408734',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5144ca0ceba9c7_18408734')) {function content_5144ca0ceba9c7_18408734($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <title>Replicator | Team Re-Factor</title>
        
            <meta content="Richard Brooks" name="author">
            <meta content="Lebowski ipsum where&#8217;s my goddamn money, you bum?! Dolor sit amet, consectetur adipiscing elit praesent ac magna justo pellentesque ac. I mean &hellip;" name="description">
            <!-- http://t.co/dKP3o1e -->
            <meta content="True" name="HandheldFriendly">
            <meta content="320" name="MobileOptimized">
            <meta content="width=device-width, initial-scale=1" name="viewport">
            <link href="stylesheets/screen.css" media="screen, projection" rel="stylesheet" type="text/css"><!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
            <!--<link href="stylesheets/bootstrap.css" media="screen, projection" rel="stylesheet" type="text/css">-->
            <!--<link href="stylesheets/flat-ui.css" media="screen, projection" rel="stylesheet" type="text/css">-->
            <!--<link href="stylesheets/override.css" media="screen, projection" rel="stylesheet" type="text/css">-->
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
            <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700' rel='stylesheet' type='text/css'>
            <!--Fonts from Google"s Web font directory at http://google.com/webfonts -->
            <link href="http://fonts.googleapis.com/css?family=PT+Serif:regular,italic,bold,bolditalic" rel="stylesheet" type="text/css">
            <link href="http://fonts.googleapis.com/css?family=PT+Sans:regular,italic,bold,bolditalic" rel="stylesheet" type="text/css">
            <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
            <link href='http://fonts.googleapis.com/css?family=VT323' rel='stylesheet' type='text/css'>
            <link rel="icon" type="image/png" href="favicon.png">
        
    </head>
    <body>
        <div class="container">
            <div class="left-col">
                <div class="intrude-less">
                    <header class="inner" id="header">
                        <a href="/wow"><img src="logo.png"></a>
                        <div style="display:none">
                            <h1><a href="/wow">Replicator</a></h1>
                            <p class="subtitle">Team Re-Factor</p>
                        </div>
                        <nav id="main-nav">
                            <ul class="main-navigation">
                                <?php if (!isset($_SESSION['UserID'])){?>
                                <li><a href="login.php">Login</a></li>
                                <?php }?>
                                
                                <?php if (isset($_SESSION['UserID'])){?>
                                <li><a href="search.php">Search</a></li>
                                <li><a href="inventory.php">Items</a></li>
                                <?php }?>
                                <?php if (isset($_SESSION['UserID'])){?>
                                <li><a href="vendors.php">Vendors</a></li>
                                <li><a href="setLocation.php">Locate</a></li>
                                <li><a href="logout.php">Logout</a></li>
                                <?php }?>
                            </ul>
                        </nav>
                        <nav id="sub-nav"></nav>
                    </header>
                </div>
            </div>
            <div class="mid-col">
                <div class="mid-col-container">
                    <div class="inner" id="content">
                        <article class="post">
                            <h1 class="title">Create Item</h1>
                            <div class="entry-content">
                                
<form action="api.php" method="get">
<input type="hidden" name="method" value="addItem">
<strong>Name:</strong><br /> <input type="text" name="name" required><br>
<strong>Description:</strong><br /> <input type="text" name="description" required><br>
<strong>Vendor Name:</strong><br /> <input type="text" name="vendorname" required><br>
<strong>Scenario:</strong><br /> <input type="text" name="scenario" list="scenarios" required><br>
<datalist id="scenarios">
   <option value="Morning">
   <option value="Day">
   <option value="Night">
</datalist>
<strong>Class:</strong><br /> <input type="text" name="class" list="itemClasses" required><br>
<datalist id="itemClasses">
   <option value="SMG">
   <option value="Pistol">
   <option value="Shotgun">
   <option value="Eridian">
   <option value="Drink">
   <option value="Sniper Rifle">
   <option value="Shield">
</datalist>
<strong>Cost:</strong><br /> <input type="text" name="cost" required><br>
<input class="btn" type="submit" value="Create">
</form>

                            </div>
                        </article>
                    </div>
                </div>
                <footer class="inner" id="footer">
                    <p>
                        &copy; 2013 Richard Brooks |
                        <span class="credit">Based on
                            <a href="http://shashankmehta.in/archive/2012/greyshade.html">Greyshade</a></span>
                    </p>
                </footer>
            </div>
        </div>
    </body>
</html><?php }} ?>