<?php /* Smarty version Smarty-3.1.13, created on 2013-03-18 01:15:30
         compiled from "/var/www/sites/yare/wow/smarty/templates/editItem.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8263336015144bccf15df56-35269531%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '29fc9f70b3660ea6b570b5ea487028cd78adc864' => 
    array (
      0 => '/var/www/sites/yare/wow/smarty/templates/editItem.tpl',
      1 => 1363551801,
      2 => 'file',
    ),
    'fc05a559fcc48881c073fded6a8797d44a7c3a5f' => 
    array (
      0 => '/var/www/sites/yare/wow/smarty/templates/layout.tpl',
      1 => 1363554592,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8263336015144bccf15df56-35269531',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5144bccf229c40_41129521',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5144bccf229c40_41129521')) {function content_5144bccf229c40_41129521($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <title>Replicator | Team Re-Factor</title>
        
            <meta content="Richard Brooks" name="author">
            <meta content="Lebowski ipsum where&#8217;s my goddamn money, you bum?! Dolor sit amet, consectetur adipiscing elit praesent ac magna justo pellentesque ac. I mean &hellip;" name="description">
            <!-- http://t.co/dKP3o1e -->
            <meta content="True" name="HandheldFriendly">
            <meta content="320" name="MobileOptimized">
            <meta content="width=device-width, initial-scale=1" name="viewport">
            <link href="stylesheets/screen.css" media="screen, projection" rel="stylesheet" type="text/css"><!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
            <!--<link href="stylesheets/bootstrap.css" media="screen, projection" rel="stylesheet" type="text/css">-->
            <!--<link href="stylesheets/flat-ui.css" media="screen, projection" rel="stylesheet" type="text/css">-->
            <!--<link href="stylesheets/override.css" media="screen, projection" rel="stylesheet" type="text/css">-->
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
            <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700' rel='stylesheet' type='text/css'>
            <!--Fonts from Google"s Web font directory at http://google.com/webfonts -->
            <link href="http://fonts.googleapis.com/css?family=PT+Serif:regular,italic,bold,bolditalic" rel="stylesheet" type="text/css">
            <link href="http://fonts.googleapis.com/css?family=PT+Sans:regular,italic,bold,bolditalic" rel="stylesheet" type="text/css">
            <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
            <link href='http://fonts.googleapis.com/css?family=VT323' rel='stylesheet' type='text/css'>
            <link rel="icon" type="image/png" href="favicon.png">
        
    </head>
    <body>
        <div class="container">
            <div class="left-col">
                <div class="intrude-less">
                    <header class="inner" id="header">
                        <a href="/wow"><img src="logo.png"></a>
                        <div style="display:none">
                            <h1><a href="/wow">Replicator</a></h1>
                            <p class="subtitle">Team Re-Factor</p>
                        </div>
                        <nav id="main-nav">
                            <ul class="main-navigation">
                                <?php if (!isset($_SESSION['UserID'])){?>
                                <li><a href="login.php">Login</a></li>
                                <?php }?>
                                
                                <?php if (isset($_SESSION['UserID'])){?>
                                <li><a href="search.php">Search</a></li>
                                <li><a href="inventory.php">Items</a></li>
                                <?php }?>
                                <?php if (isset($_SESSION['UserID'])){?>
                                <li><a href="vendors.php">Vendors</a></li>
                                <li><a href="setLocation.php">Locate</a></li>
                                <li><a href="logout.php">Logout</a></li>
                                <?php }?>
                            </ul>
                        </nav>
                        <nav id="sub-nav"></nav>
                    </header>
                </div>
            </div>
            <div class="mid-col">
                <div class="mid-col-container">
                    <div class="inner" id="content">
                        <article class="post">
                            <h1 class="title">Edit Item</h1>
                            <div class="entry-content">
                                
<input value="Add Image" type="submit" class='btn' onclick="filepicker.pickAndStore({mimetype:'image/*'},
        {location:'S3'}, function(fpfiles){
   console.log(fpfiles);
   window.location.href = 'http://yare.us/wow/api.php?method=addItemImage&itemID=<?php echo $_smarty_tpl->tpl_vars['itemID']->value;?>
&itemImage=' + fpfiles[0].url.substr(fpfiles[0].url.lastIndexOf('/')+1, fpfiles[0].url.length) + '&fromTechnician=<?php if ($_SESSION['isTechnician']==true){?>true<?php }else{ ?>false<?php }?>';
});"></input><br />
<?php if ($_SESSION['isTechnician']){?>
    <form action="api.php" method="get">
        <input type="hidden" name="method" value="editItem">
        <input type="hidden" name="itemID" value="<?php echo $_smarty_tpl->tpl_vars['itemID']->value;?>
">
        <strong>Name:</strong> <br /><input type="text" name="name" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['Name'];?>
" required><br>
        <strong>Description:</strong> <br /><textarea style="height: 5em; width:250px" type="text" name="description" required><?php echo $_smarty_tpl->tpl_vars['item']->value['Description'];?>
</textarea><br>
        <strong>Vendor Name:</strong> <br /><input type="text" name="vendorname" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['VendorName'];?>
" required><br>
        <strong>Scenario:</strong> <br /><input type="text" name="scenario" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['Scenario'];?>
" list="scenarios" required><br>
        <datalist id="scenarios">
           <option value="Morning">
           <option value="Day">
           <option value="Night">
        </datalist>
        <strong>Class:</strong> <br /><input type="text" name="class" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['Class'];?>
" list="itemClasses" required><br>
        <datalist id="itemClasses">
           <option value="SMG">
           <option value="Pistol">
           <option value="Shotgun">
           <option value="Eridian">
           <option value="Drink">
           <option value="Sniper Rifle">
           <option value="Shield">
        </datalist>
        <strong>Cost:</strong> <br /><input type="number" name="cost" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['Cost'];?>
" required><br>
        <input type="submit" value="Save" class="btn"><br />
        <strong>Attributes:</strong>
        
    </form>
<?php }else{ ?>
    <strong>Name:</strong> <br /><?php echo $_smarty_tpl->tpl_vars['item']->value['Name'];?>
 <br />
    <strong>Description:</strong> <br /><?php echo $_smarty_tpl->tpl_vars['item']->value['Description'];?>
<br />
    <strong>Vendor Name:</strong> <br /><?php echo $_smarty_tpl->tpl_vars['item']->value['VendorName'];?>
<br />
    <strong>Scenario:</strong> <br /><?php echo $_smarty_tpl->tpl_vars['item']->value['Scenario'];?>
<br />
    <strong>Class:</strong> <br /><?php echo $_smarty_tpl->tpl_vars['item']->value['Class'];?>
<br />
    <strong>Cost:</strong> <br /><?php echo $_smarty_tpl->tpl_vars['item']->value['Cost'];?>
<br />
    <strong>Attrbutes:</strong><br />
<?php }?>
<ul style="list-style-type: none; margin-left: 0">
<?php  $_smarty_tpl->tpl_vars['attribute'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['attribute']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['itemAttributes']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['attribute']->key => $_smarty_tpl->tpl_vars['attribute']->value){
$_smarty_tpl->tpl_vars['attribute']->_loop = true;
?>
    <li><input style="width:55px; margin-right:10px" type="submit" value="<?php echo $_smarty_tpl->tpl_vars['attribute']->value['Score'];?>
" class="btn btn-large disabled"><strong style="width: 5em; display: inline-block; overflow:hidden; text-overflow:ellipsis;"><?php echo $_smarty_tpl->tpl_vars['attribute']->value['AttributeName'];?>
</strong><input style="width:35px; margin-right:0" type="submit" value="-1" class="btn" onclick="location.href= 'api.php?method=downVoteAttribute&attributeID=<?php echo $_smarty_tpl->tpl_vars['attribute']->value['AttributeID'];?>
'; return false;"><input style="width:35px; margin-right:0" type="submit" value="+1" class="btn" onclick="location.href= 'api.php?method=upVoteAttribute&attributeID=<?php echo $_smarty_tpl->tpl_vars['attribute']->value['AttributeID'];?>
'; return false;"><?php if ($_smarty_tpl->tpl_vars['attribute']->value['Score']<=0){?><input style="width:35px" type="submit" value="X" class="btn btn-danger" onclick="location.href= 'api.php?method=deleteAttribute&attributeID=<?php echo $_smarty_tpl->tpl_vars['attribute']->value['AttributeID'];?>
'; return false;"><?php }?></li>
<?php } ?>
</ul>
<form action="api.php" method="get">
<input type="hidden" name="method" value="addAttribute">
<input type="hidden" name="itemID" value="<?php echo $_smarty_tpl->tpl_vars['itemID']->value;?>
">
<input type="text" name="attributename" list="attributeNames">
<datalist id="attributeNames">
    <option value="Poison">
    <option value="Fire">
    <option value="Explosion">
    <option value="Eridian">
    <option value="Corrosive">
    <option value="Shock">
    <option value="Slag">
</datalist>
<br />
<input type="submit" value="New Attribute" class="btn">

<br />


<script type="text/javascript">
function loadScript() {
    filepicker.setKey('AzGFiOmvMSR2sEYoBUJulz');
}

window.onload = loadScript;

(function(a){if(window.filepicker){return}var b=a.createElement("script");b.type="text/javascript";b.async=!0;b.src=("https:"===a.location.protocol?"https:":"http:")+"//api.filepicker.io/v1/filepicker.js";var c=a.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c);var d={};d._queue=[];var e="pick,pickMultiple,pickAndStore,read,write,writeUrl,export,convert,store,storeUrl,remove,stat,setKey,constructWidget,makeDropPane".split(",");var f=function(a,b){return function(){b.push([a,arguments])}};for(var g=0;g<e.length;g++){d[e[g]]=f(e[g],d._queue)}window.filepicker=d})(document); 
</script>


                            </div>
                        </article>
                    </div>
                </div>
                <footer class="inner" id="footer">
                    <p>
                        &copy; 2013 Richard Brooks |
                        <span class="credit">Based on
                            <a href="http://shashankmehta.in/archive/2012/greyshade.html">Greyshade</a></span>
                    </p>
                </footer>
            </div>
        </div>
    </body>
</html><?php }} ?>