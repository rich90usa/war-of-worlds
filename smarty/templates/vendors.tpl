{extends file='layout.tpl'}

{block name=pageTitle}Vendors{/block}

{block name=pageContent}
<div id="map-canvas" style="height:300px"></div><br />
{if $smarty.session.isTechnician}<input type="submit" value="Add Vendor" class="btn" onclick="location.href= 'addVendor.php'; return false;"></input>{/if}
<ul style="list-style-type: none; margin-left: 0">
{foreach from=$vendors item=vendor}
    <li class="btn disabled" style="background-color: whitesmoke; padding:0;"><input style="width:220px" type="submit" value="{$vendor['VendorName']}" class="btn btn-large {if $smarty.session.UserID === $vendor['Owner']}btn-success{/if}" onclick="location.href= 'vendor.php?vendorID={$vendor['VendorID']}'; return false;"></input><input style="width:100px" type="submit" value="Edit" class="btn" onclick="location.href= 'editVendor.php?vendorID={$vendor['VendorID']}'; return false;"></input>{if $smarty.session.UserID === $vendor['Owner']}<input style="width:100px" type="submit" value="Delete" class="btn btn-danger" onclick="location.href= 'api.php?method=deleteVendor&vendorID={$vendor['VendorID']}'; return false;"></input>{/if}</li>
{/foreach}
</ul>

<script type="text/javascript">
function initialize() {
  var mapOptions = {
    zoom: 13,
    center: new google.maps.LatLng(29.650242, -82.316093),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

  var markers = [
    {foreach from=$vendors item=vendor}
        new google.maps.Marker({
            map:map,
            icon: {
              path: google.maps.SymbolPath.CIRCLE,
              fillColor: 'white',
              strokeColor: 'red',
              fillOpacity: 1.0,
              strokeOpacity: 1.0,
              strokeWeight: 4.0,
              scale: 5
            },
            draggable:false,
            //animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng({$vendor['Latitude']}, {$vendor['Longitude']})
        }),
    {/foreach}
  ];
  var userMarker = 
        new google.maps.Marker({
            map:map,
            icon: {
              path: google.maps.SymbolPath.CIRCLE,
              fillColor: 'red',
              strokeColor: 'white',
              fillOpacity: 1.0,
              strokeOpacity: 1.0,
              strokeWeight: 4.0,
              scale: 5
            },
            draggable:false,
            //animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng({$smarty.session.Latitude}, {$smarty.session.Longitude})
        });
}

function loadScript() {
  var script = document.createElement("script");
  script.type = "text/javascript";
  script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyCv2K7qIWGVpygfRaf9rqspLFR-fSfnlzU&sensor=true&callback=initialize";
  document.body.appendChild(script);
}


window.onload = loadScript;
</script>
{/block}