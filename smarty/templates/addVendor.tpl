{extends file='layout.tpl'}

{block name=pageTitle}Create Vendor{/block}

{block name=pageContent}
{if $smarty.session.isTechnician}
    <form action="api.php" method="get">
    <input type="hidden" name="method" value="addVendor">
    <strong>Vendor Name:</strong><br /> <input type="text" name="vendorname" required><br>
    <strong>Latitude:</strong><br /> <input type="text" name="latitude" required><br>
    <strong>Longitude:</strong><br /> <input type="text" name="longitude" required><br>
    <strong>Address:</strong><br /> <input type="text" name="address" required><br>
    <strong>Phone:</strong><br /> <input type="text" name="phone" required><br>
    <strong>EMail:</strong><br /> <input type="text" name="email" required><br>
    <input class="btn" type="submit" value="Create">
    </form>
{else}
    You need to be logged in as a technician
{/if}
{/block}