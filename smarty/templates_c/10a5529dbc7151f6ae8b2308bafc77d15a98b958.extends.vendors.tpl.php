<?php /* Smarty version Smarty-3.1.13, created on 2013-03-18 01:11:08
         compiled from "/var/www/sites/yare/wow/smarty/templates/vendors.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16159416785144b62490e982-88488443%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '10a5529dbc7151f6ae8b2308bafc77d15a98b958' => 
    array (
      0 => '/var/www/sites/yare/wow/smarty/templates/vendors.tpl',
      1 => 1363554001,
      2 => 'file',
    ),
    'fc05a559fcc48881c073fded6a8797d44a7c3a5f' => 
    array (
      0 => '/var/www/sites/yare/wow/smarty/templates/layout.tpl',
      1 => 1363554592,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16159416785144b62490e982-88488443',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5144b624a02b38_36813945',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5144b624a02b38_36813945')) {function content_5144b624a02b38_36813945($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <title>Replicator | Team Re-Factor</title>
        
            <meta content="Richard Brooks" name="author">
            <meta content="Lebowski ipsum where&#8217;s my goddamn money, you bum?! Dolor sit amet, consectetur adipiscing elit praesent ac magna justo pellentesque ac. I mean &hellip;" name="description">
            <!-- http://t.co/dKP3o1e -->
            <meta content="True" name="HandheldFriendly">
            <meta content="320" name="MobileOptimized">
            <meta content="width=device-width, initial-scale=1" name="viewport">
            <link href="stylesheets/screen.css" media="screen, projection" rel="stylesheet" type="text/css"><!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
            <!--<link href="stylesheets/bootstrap.css" media="screen, projection" rel="stylesheet" type="text/css">-->
            <!--<link href="stylesheets/flat-ui.css" media="screen, projection" rel="stylesheet" type="text/css">-->
            <!--<link href="stylesheets/override.css" media="screen, projection" rel="stylesheet" type="text/css">-->
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
            <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700' rel='stylesheet' type='text/css'>
            <!--Fonts from Google"s Web font directory at http://google.com/webfonts -->
            <link href="http://fonts.googleapis.com/css?family=PT+Serif:regular,italic,bold,bolditalic" rel="stylesheet" type="text/css">
            <link href="http://fonts.googleapis.com/css?family=PT+Sans:regular,italic,bold,bolditalic" rel="stylesheet" type="text/css">
            <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
            <link href='http://fonts.googleapis.com/css?family=VT323' rel='stylesheet' type='text/css'>
            <link rel="icon" type="image/png" href="favicon.png">
        
    </head>
    <body>
        <div class="container">
            <div class="left-col">
                <div class="intrude-less">
                    <header class="inner" id="header">
                        <a href="/wow"><img src="logo.png"></a>
                        <div style="display:none">
                            <h1><a href="/wow">Replicator</a></h1>
                            <p class="subtitle">Team Re-Factor</p>
                        </div>
                        <nav id="main-nav">
                            <ul class="main-navigation">
                                <?php if (!isset($_SESSION['UserID'])){?>
                                <li><a href="login.php">Login</a></li>
                                <?php }?>
                                
                                <?php if (isset($_SESSION['UserID'])){?>
                                <li><a href="search.php">Search</a></li>
                                <li><a href="inventory.php">Items</a></li>
                                <?php }?>
                                <?php if (isset($_SESSION['UserID'])){?>
                                <li><a href="vendors.php">Vendors</a></li>
                                <li><a href="setLocation.php">Locate</a></li>
                                <li><a href="logout.php">Logout</a></li>
                                <?php }?>
                            </ul>
                        </nav>
                        <nav id="sub-nav"></nav>
                    </header>
                </div>
            </div>
            <div class="mid-col">
                <div class="mid-col-container">
                    <div class="inner" id="content">
                        <article class="post">
                            <h1 class="title">Vendors</h1>
                            <div class="entry-content">
                                
<div id="map-canvas" style="height:300px"></div><br />
<?php if ($_SESSION['isTechnician']){?><input type="submit" value="Add Vendor" class="btn" onclick="location.href= 'addVendor.php'; return false;"></input><?php }?>
<ul style="list-style-type: none; margin-left: 0">
<?php  $_smarty_tpl->tpl_vars['vendor'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['vendor']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['vendors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['vendor']->key => $_smarty_tpl->tpl_vars['vendor']->value){
$_smarty_tpl->tpl_vars['vendor']->_loop = true;
?>
    <li class="btn disabled" style="background-color: whitesmoke; padding:0;"><input style="width:220px" type="submit" value="<?php echo $_smarty_tpl->tpl_vars['vendor']->value['VendorName'];?>
" class="btn btn-large <?php if ($_SESSION['UserID']===$_smarty_tpl->tpl_vars['vendor']->value['Owner']){?>btn-success<?php }?>" onclick="location.href= 'vendor.php?vendorID=<?php echo $_smarty_tpl->tpl_vars['vendor']->value['VendorID'];?>
'; return false;"></input><input style="width:100px" type="submit" value="Edit" class="btn" onclick="location.href= 'editVendor.php?vendorID=<?php echo $_smarty_tpl->tpl_vars['vendor']->value['VendorID'];?>
'; return false;"></input><?php if ($_SESSION['UserID']===$_smarty_tpl->tpl_vars['vendor']->value['Owner']){?><input style="width:100px" type="submit" value="Delete" class="btn btn-danger" onclick="location.href= 'api.php?method=deleteVendor&vendorID=<?php echo $_smarty_tpl->tpl_vars['vendor']->value['VendorID'];?>
'; return false;"></input><?php }?></li>
<?php } ?>
</ul>

<script type="text/javascript">
function initialize() {
  var mapOptions = {
    zoom: 13,
    center: new google.maps.LatLng(29.650242, -82.316093),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

  var markers = [
    <?php  $_smarty_tpl->tpl_vars['vendor'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['vendor']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['vendors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['vendor']->key => $_smarty_tpl->tpl_vars['vendor']->value){
$_smarty_tpl->tpl_vars['vendor']->_loop = true;
?>
        new google.maps.Marker({
            map:map,
            icon: {
              path: google.maps.SymbolPath.CIRCLE,
              fillColor: 'white',
              strokeColor: 'red',
              fillOpacity: 1.0,
              strokeOpacity: 1.0,
              strokeWeight: 4.0,
              scale: 5
            },
            draggable:false,
            //animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(<?php echo $_smarty_tpl->tpl_vars['vendor']->value['Latitude'];?>
, <?php echo $_smarty_tpl->tpl_vars['vendor']->value['Longitude'];?>
)
        }),
    <?php } ?>
  ];
  var userMarker = 
        new google.maps.Marker({
            map:map,
            icon: {
              path: google.maps.SymbolPath.CIRCLE,
              fillColor: 'red',
              strokeColor: 'white',
              fillOpacity: 1.0,
              strokeOpacity: 1.0,
              strokeWeight: 4.0,
              scale: 5
            },
            draggable:false,
            //animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(<?php echo $_SESSION['Latitude'];?>
, <?php echo $_SESSION['Longitude'];?>
)
        });
}

function loadScript() {
  var script = document.createElement("script");
  script.type = "text/javascript";
  script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyCv2K7qIWGVpygfRaf9rqspLFR-fSfnlzU&sensor=true&callback=initialize";
  document.body.appendChild(script);
}


window.onload = loadScript;
</script>

                            </div>
                        </article>
                    </div>
                </div>
                <footer class="inner" id="footer">
                    <p>
                        &copy; 2013 Richard Brooks |
                        <span class="credit">Based on
                            <a href="http://shashankmehta.in/archive/2012/greyshade.html">Greyshade</a></span>
                    </p>
                </footer>
            </div>
        </div>
    </body>
</html><?php }} ?>