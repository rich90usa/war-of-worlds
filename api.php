<?php

require_once('./lib/limonade/lib/limonade.php');

require_once('./methods.php');

session_start();

function configure()
{
  option('session', 'wowzomgit'); // enable with a specific session name
}

function before($route)
{
    if ($route['callback'] == 'api_post' || $route['callback'] == 'api') {
        $db = getDB();
        mysql_select_db('wowdb', $db);
    }
}

function after($output, $route) {
    if (isset($db)) {
        closeDB($db);
    }
    return $output;
}

dispatch('/', 'api');
    function api() {
        $method = null;
        
        if (isset($_GET['method'])) {
            $method = $_GET['method'];
        }
        
        if ($method == 'getAllItems') {
            return json(getAllItems());
        }
        if ($method == 'getItemByID') {
            if (isset($_GET['itemID'])) {
                $itemID = (int) $_GET['itemID'];
            } else {
                return json(array('result' => array('error' => 'Invalid or no itemID')));
            }

            return json(getItemByID($itemID));
        }
        if ($method == 'searchItemsByName') {
            if (isset($_GET['name'])) {
                $searchName = $_GET['name'];
            } else {
                return json(array('result' => array('error' => 'Invalid or non-existant search name')));
            }

            return json(searchItemsByName($searchName));
        }
        if ($method == 'deleteItem') {
            if (isset($_GET['itemID'])) {
                $itemID = (int) $_GET['itemID'];
            } else {
                return json(array('result' => array('error' => 'No itemID provided')));
            }

            $result = json(deleteItem($itemID));
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
        if ($method == 'addItemImage') {
            if (isset($_GET['itemID'])) {
                $itemID = $_GET['itemID'];
            } else {
                return json(array('result' => array('error' => 'No itemID Provided')));
            }
            if (isset($_GET['itemImage'])) {
                $imageURL = $_GET['itemImage'];
            } else {
                return json(array('result' => array('error' => 'No itemImage (End of URL) Provided')));
            }
            if (isset($_GET['fromTechnician'])) {
                $fromTechnician = $_GET['fromTechnician'];
                if ($fromTechnician === true) {
                    $fromTechnician = 1;
                } else {
                    $fromTechnician = 0;
                }
            } else {
                return json(array('result' => array('error' => 'No fromTechnician Provided')));
            }

            $result = json(addItemImage($itemID, $imageURL, $fromTechnician));
            header("Location: http://yare.us/wow/item.php?itemID=".$itemID);
        }
        if ($method == 'addItem') {
            if (isset($_GET['name'])) {
                $name = $_GET['name'];
            } else {
                return json(array('result' => array('error' => 'No Name Provided')));
            }
            if (isset($_GET['description'])) {
                $description = $_GET['description'];
            } else {
                return json(array('result' => array('error' => 'No Description Provided')));
            }
            if (isset($_GET['vendorname'])) {
                $vendorname = $_GET['vendorname'];
            } else {
                return json(array('result' => array('error' => 'No Vendor Name Provided')));
            }
            if (isset($_GET['scenario'])) {
                $scenario = $_GET['scenario'];
            } else {
                return json(array('result' => array('error' => 'No Scenario Provided')));
            }
            if (isset($_GET['class'])) {
                $class = $_GET['class'];
            } else {
                return json(array('result' => array('error' => 'No Class Provided')));
            }
            if (isset($_GET['cost'])) {
                $cost = (int) $_GET['cost'];
            } else {
                return json(array('result' => array('error' => 'No Cost Provided')));
            }

            $result = json(addItem($name, $description, $vendorname, $scenario, $class, $cost));
            header("Location: http://yare.us/wow/inventory.php");
        }
        if ($method == 'editItem') {
            if (isset($_GET['itemID'])) {
                $itemID = (int) $_GET['itemID'];
            } else {
                return json(array('result' => array('error' => 'No itemID Provided')));
            }
            if (isset($_GET['name'])) {
                $name = $_GET['name'];
            } else {
                return json(array('result' => array('error' => 'No Name Provided')));
            }
            if (isset($_GET['description'])) {
                $description = $_GET['description'];
            } else {
                return json(array('result' => array('error' => 'No Description Provided')));
            }
            if (isset($_GET['vendorname'])) {
                $vendorname = $_GET['vendorname'];
            } else {
                return json(array('result' => array('error' => 'No Vendor Name Provided')));
            }
            if (isset($_GET['scenario'])) {
                $scenario = $_GET['scenario'];
            } else {
                return json(array('result' => array('error' => 'No Scenario Provided')));
            }
            if (isset($_GET['class'])) {
                $class = $_GET['class'];
            } else {
                return json(array('result' => array('error' => 'No Class Provided')));
            }
            if (isset($_GET['cost'])) {
                $cost = (int) $_GET['cost'];
            } else {
                return json(array('result' => array('error' => 'No Cost Provided')));
            }

            $result = editItem($itemID, $name, $description, $vendorname, $scenario, $class, $cost);
            header("Location: http://yare.us/wow/item.php?itemID=".$itemID);
        }
        if ($method == 'addVendor') {
            if (isset($_GET['vendorname'])) {
                $vendorName = $_GET['vendorname'];
            } else {
                return json(array('result' => array('error' => 'No Vendor Name Provided')));
            }
            if (isset($_GET['latitude'])) {
                $latitude = $_GET['latitude'];
            } else {
                return json(array('result' => array('error' => 'No Latitude Provided')));
            }
            if (isset($_GET['longitude'])) {
                $longitude = $_GET['longitude'];
            } else {
                return json(array('result' => array('error' => 'No Longitude Provided')));
            }
            if (isset($_GET['address'])) {
                $address = $_GET['address'];
            } else {
                return json(array('result' => array('error' => 'No address Provided')));
            }
            if (isset($_GET['phone'])) {
                $phone = $_GET['phone'];
            } else {
                return json(array('result' => array('error' => 'No phone Provided')));
            }
            if (isset($_GET['email'])) {
                $email= $_GET['email'];
            } else {
                return json(array('result' => array('error' => 'No email Provided')));
            }

            $result = json(addVendor($vendorName, $latitude, $longitude, $address, $phone, $email));
            header("Location: http://yare.us/wow/vendors.php");
        }
        if ($method == 'addAttribute') {
            if (isset($_GET['attributename'])) {
                $attributeName = $_GET['attributename'];
            } else {
                return json(array('result' => array('error' => 'No attributename provided')));
            }
            if (isset($_GET['itemID'])) {
                $itemID = $_GET['itemID'];
            } else {
                return json(array('result' => array('error' => 'No itemID Provided')));
            }

            $result = json(addAttribute($itemID, $attributeName));
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
        if ($method == 'setLocation') {
            if (isset($_GET['latitude'])) {
                $latitude = $_GET['latitude'];
            } else {
                return json(array('result' => array('error' => 'No latitude provided')));
            }
            if (isset($_GET['longitude'])) {
                $longitude = $_GET['longitude'];
            } else {
                return json(array('result' => array('error' => 'No longitude Provided')));
            }

            header("Location: http://yare.us/wow");
            return json(setLocation($latitude, $longitude));
        }
        if ($method == 'upVoteAttribute') {
            if (isset($_GET['attributeID'])) {
                $attributeID = $_GET['attributeID'];
            } else {
                return json(array('result' => array('error' => 'No attributeID Provided')));
            }

            $result = json(upVoteAttribute($attributeID));
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
        if ($method == 'downVoteAttribute') {
            if (isset($_GET['attributeID'])) {
                $attributeID = $_GET['attributeID'];
            } else {
                return json(array('result' => array('error' => 'No attributeID Provided')));
            }

            $result = json(downVoteAttribute($attributeID));
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
        if ($method == 'deleteAttribute') {
            if (isset($_GET['attributeID'])) {
                $attributeID = $_GET['attributeID'];
            } else {
                return json(array('result' => array('error' => 'No attributeID Provided')));
            }

            $result = json(deleteAttribute($attributeID));
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
        if ($method == 'editVendor') {
            if (isset($_GET['vendorID'])) {
                $vendorID = (int) $_GET['vendorID'];
            } else {
                return json(array('result' => array('error' => 'No vendorID Provided')));
            }
            if (isset($_GET['vendorname'])) {
                $vendorName = $_GET['vendorname'];
            } else {
                return json(array('result' => array('error' => 'No Name Provided')));
            }
            if (isset($_GET['latitude'])) {
                $latitude = $_GET['latitude'];
            } else {
                return json(array('result' => array('error' => 'No Latitude Provided')));
            }
            if (isset($_GET['longitude'])) {
                $longitude = $_GET['longitude'];
            } else {
                return json(array('result' => array('error' => 'No Longitude Provided')));
            }
            if (isset($_GET['address'])) {
                $address = $_GET['address'];
            } else {
                return json(array('result' => array('error' => 'No address Provided')));
            }
            if (isset($_GET['phone'])) {
                $phone = $_GET['phone'];
            } else {
                return json(array('result' => array('error' => 'No phone Provided')));
            }
            if (isset($_GET['email'])) {
                $email= $_GET['email'];
            } else {
                return json(array('result' => array('error' => 'No email Provided')));
            }

            editVendor($vendorID, $vendorName, $latitude, $longitude, $address, $phone, $email);
            header("Location: http://yare.us/wow/vendor.php?vendorID=".$vendorID);
        }
        if ($method == 'deleteVendor') {
            if (isset($_GET['vendorID'])) {
                $vendorID = (int) $_GET['vendorID'];
            } else {
                return json(array('result' => array('error' => 'No itemID provided')));
            }

            $result = json(deleteVendor($vendorID));
            header("Location: http://yare.us/wow/vendors.php");
        }
        if ($method == 'claimVendor') {
            if (isset($_GET['vendorID'])) {
                $vendorID = $_GET['vendorID'];
            } else {
                return json(array('result' => array('error' => 'Need vendorID')));
            }

            $result = json(claimVendor($vendorID));
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
        if ($method == 'disownVendor') {
            if (isset($_GET['vendorID'])) {
                $vendorID = $_GET['vendorID'];
            } else {
                return json(array('result' => array('error' => 'Need vendorID')));
            }

            $result = json(disownVendor($vendorID));
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
        else {
            return json(array('result' => array('error' => 'Invalid API Method')));
        }    
    }

dispatch_post('/', 'api_post');
    function api_post() {
        $method = null;

        if (isset($_REQUEST['method'])) {
            $method = $_REQUEST['method'];
        }
        if ($method == 'test') {
            return json(array('result' => array('data' => 'ffuckyoufuckyoufuckyoufuckyouuckyou')));
        }
        if ($method == 'login') {
            if (isset($_REQUEST['username'])) {
                $username = $_REQUEST['username'];
            }
            if (isset($_REQUEST['password'])) {
                $password = $_REQUEST['password'];
            }
            $result = login($username, $password);
            if ($result['UserID']) {
                header("Location: http://yare.us/wow/setLocation.php");
                return json(array('result' => array('success' => true)));
            } else {
                $result = json(array('result' => array('error' => 'Login Failed')));
                header('Location: ' . $_SERVER['HTTP_REFERER']);
            }
        }
        if ($method == 'register') {
            if (isset($_REQUEST['username'])) {
                $username = $_REQUEST['username'];
            }
            if (isset($_REQUEST['password'])) {
                $password = $_REQUEST['password'];
            }
            if (isset($_REQUEST['isTechnician'])) {
                $isTechnician = $_REQUEST['isTechnician'];
                if ($isTechnician === 'on') {
                    $isTechnician = true;
                } 
            } else {
                $isTechnician = false;
            }
            $result = register($username, $password, $isTechnician);
            header("Location: http://yare.us/wow/setLocation.php");
        }
        else {
            return json(array('result' => array('error' => 'Invalid API Method')));
        }
    }

# RUN THE APP!
run();

?>
