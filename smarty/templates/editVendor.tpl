{extends file='layout.tpl'}

{block name=pageTitle}Edit Vendor{/block}

{block name=pageContent}
{if $smarty.session.isTechnician}
    <form action="api.php" method="get">
        <input type="hidden" name="method" value="editVendor">
        <input type="hidden" name="vendorID" value="{$vendorID}">
        Vendor Name: <br /><input type="text" name="vendorname" value="{$vendor['VendorName']}" required><br>
        Latitude: <br /><input type="number" step=".000000001" name="latitude" value="{$vendor['Latitude']}" required><br>
        Longitude: <br /><input type="number" step=".000000001" name="longitude" value="{$vendor['Longitude']}" required><br>
        Address: <br /><input type="text" name="address" value="{$vendor['Address']}" required><br>
        Phone: <br /><input type="number" name="phone" value="{$vendor['Phone']}" required><br>
        EMail: <br /><input type="email" name="email" value="{$vendor['EMail']}" required><br>
        <input class="btn" type="submit" value="Save">
    </form>
    {if $smarty.session.UserID === $vendor['Owner']}
    <input type="submit" value="Disown" class="btn btn-danger" onclick="location.href= 'api.php?method=disownVendor&vendorID={$vendor['VendorID']}'; return false;"></input>
    {elseif $vendor['Owner'] lte 0}
    <input type="submit" value="Claim" class="btn btn-success" onclick="location.href= 'api.php?method=claimVendor&vendorID={$vendor['VendorID']}'; return false;"></input>
    {/if}
{else}
    You need to be logged in as a technician.
{/if}
{/block}