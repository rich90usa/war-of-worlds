<?php

require_once('./smarty/smarty_main.inc');
require_once('./methods.php');

$db = getDB();

if (isset($_GET['name'])) {
    $searchQuery = $_GET['name'];

    $potentialPriceQuery = explode('-', $searchQuery);
    if (isset($potentialPriceQuery[0]) && isset($potentialPriceQuery[1])) {
        $minQuery = (int) $potentialPriceQuery[0];
        $maxQuery = (int) $potentialPriceQuery[1];
    }
    if ($minQuery > 0 && $maxQuery > 0 && $minQuery !== $maxQuery) {
        $priceSearchResults = searchItemsByPrice($minQuery, $maxQuery);
        $searchResults = $priceSearchResults['result'];
    } else {
        $searchResults = searchItemsByName($searchQuery);
        $searchResults = $searchResults['result'];
    }

    $smarty->assign('searchQuery', $searchQuery);
    $smarty->assign('searchResults', $searchResults);
}

$smarty->display('extends:layout.tpl|search.tpl');

closeDB($db);

?>