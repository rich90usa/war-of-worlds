<?php

require_once('./smarty/smarty_main.inc');
require_once('./methods.php');

$db = getDB();

if (isset($_GET['vendorID'])) {
    $vendorID = $_GET['vendorID'];
    $vendor = getVendorByID($vendorID);
    $vendor = $vendor['result'][0];

    $items = getItemsByVendorName($vendor['VendorName']);
    $items = $items['result'];

    $smarty->assign('vendor', $vendor);
    $smarty->assign('items', $items);

    $smarty->display('extends:layout.tpl|vendor.tpl');
}

closeDB($db);

?>