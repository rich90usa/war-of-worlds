{extends file='layout.tpl'}

{block name=pageTitle}View Vendor{/block}

{block name=pageContent}
<div id="map-canvas" style="height:300px"></div><br />
{if $smarty.session.isTechnician and (($smarty.session.UserID === $vendor['Owner']) or ($vendor['Owner'] lte 0))}
<input type="submit" value="Edit" class="btn" onclick="location.href= 'editVendor.php?vendorID={$vendor['VendorID']}'; return false;"></input>
{/if}
<input type="submit" value="Navigate" class="btn btn-info" onclick="location.href= 'https://maps.google.com/maps?saddr={$smarty.session.Latitude},+{$smarty.session.Longitude}&daddr={$vendor['Latitude']},+{$vendor['Longitude']}'; return false;"></input><br />
<strong>Name:</strong><br /> {$vendor['VendorName']} <br />
<strong>Latitude:</strong><br /> {$vendor['Latitude']}<br />
<strong>Longitude:</strong><br /> {$vendor['Longitude']}<br />
<strong>Address:</strong><br /> {$vendor['Address']}<br>
<strong>Phone:</strong><br /> {$vendor['Phone']}<br>
<strong>EMail:</strong><br /> {$vendor['EMail']}<br>
{if $vendor['Owner'] > 0}
<strong>Owner:</strong><br /> Agent {$vendor['Owner']}
{else}
<strong>Owner:</strong><br /> Unclaimed
{/if}
<br />
{if !isset($items['error']) and count($items) gt 0}
    <h3>Items:</h3>
    <ul>
    {foreach from=$items item=item}
        <li class="btn disabled" style="background-color: whitesmoke; padding:0;"><input style="width:220px" type="submit" value="{$item['Name']}" class="btn btn-large" onclick="location.href= 'item.php?itemID={$item['ItemID']}'; return false;"></input><input style="width:100px" type="submit" value="Edit" class="btn" onclick="location.href= 'editItem.php?itemID={$item['ItemID']}'; return false;"></input>{if $smarty.session.isTechnician}<input style="width:100px" type="submit" value="Delete" class="btn btn-danger" onclick="location.href= 'api.php?method=deleteItem&itemID={$item['ItemID']}'; return false;">{/if}</li>
    {/foreach}
    </ul>
{else}
    {$items['error']}
{/if}
<script type="text/javascript">
function initialize() {
  var mapOptions = {
    zoom: 13,
    center: new google.maps.LatLng({$vendor['Latitude']}, {$vendor['Longitude']}),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

  var marker = 
        new google.maps.Marker({
            map:map,
            icon: {
              path: google.maps.SymbolPath.CIRCLE,
              fillColor: 'white',
              strokeColor: 'red',
              fillOpacity: 1.0,
              strokeOpacity: 1.0,
              strokeWeight: 4.0,
              scale: 5
            },
            draggable:false,
            //animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng({$vendor['Latitude']}, {$vendor['Longitude']})
        });
  var userMarker = 
        new google.maps.Marker({
            map:map,
            icon: {
              path: google.maps.SymbolPath.CIRCLE,
              fillColor: 'red',
              strokeColor: 'white',
              fillOpacity: 1.0,
              strokeOpacity: 1.0,
              strokeWeight: 4.0,
              scale: 5
            },
            title: 'User',
            draggable:false,
            //animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng({$smarty.session.Latitude}, {$smarty.session.Longitude})
        });
}

function loadScript() {
  var script = document.createElement("script");
  script.type = "text/javascript";
  script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyCv2K7qIWGVpygfRaf9rqspLFR-fSfnlzU&sensor=true&callback=initialize";
  document.body.appendChild(script);
}


window.onload = loadScript;
</script>
{/block}