{extends file='layout.tpl'}

{block name=pageTitle}Search Items{/block}

{block name=pageContent}
<form action="search.php" method="get">
    <input type="search" name="name" list="searchSuggest"><br />
    <datalist id="searchSuggest">
        <option value="Day">
        <option value="Morning">
        <option value="Night">
        <option value="Dahl">
        <option value="Tediore">
        <option value="Torgue">
        <option value="Maliwan">
        <option value="Hyperion">
        <option value="Launcher">
        <option value="SMG">
        <option value="Assault Rifle">
        <option value="Pistol">
        <option value="Shotgun">
        <option value="Sniper Rifle">
        <option value="Atlas">
        <option value="Jakobs">
        <option value="Vladof">
        <option value="Eridian">
    </datalist>
</form>
{if $searchResults}
    <strong>Results for "</strong>{$searchQuery}<strong>":</strong><br />
    <ul style="list-style-type: none; margin-left: 0">
    {foreach from=$searchResults item=item}
        <li class="btn disabled" style="background-color: whitesmoke; padding:0;"><input style="width:220px" type="submit" value="{$item['Name']}" class="btn btn-large" onclick="location.href= 'item.php?itemID={$item['ItemID']}'; return false;"></input><input style="width:100px" type="submit" value="Edit" class="btn" onclick="location.href= 'editItem.php?itemID={$item['ItemID']}'; return false;"></input>{if $smarty.session.isTechnician}<input style="width:100px" type="submit" value="Delete" class="btn btn-danger" onclick="location.href= 'api.php?method=deleteItem&itemID={$item['ItemID']}'; return false;">{/if}</li>
    {/foreach}
    </ul>
{/if}
{/block}