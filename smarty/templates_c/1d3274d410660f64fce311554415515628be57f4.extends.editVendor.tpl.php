<?php /* Smarty version Smarty-3.1.13, created on 2013-03-18 00:57:09
         compiled from "/var/www/sites/yare/wow/smarty/templates/editVendor.tpl" */ ?>
<?php /*%%SmartyHeaderCode:21008130175144b8eb8008a2-00724733%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1d3274d410660f64fce311554415515628be57f4' => 
    array (
      0 => '/var/www/sites/yare/wow/smarty/templates/editVendor.tpl',
      1 => 1363545358,
      2 => 'file',
    ),
    'fc05a559fcc48881c073fded6a8797d44a7c3a5f' => 
    array (
      0 => '/var/www/sites/yare/wow/smarty/templates/layout.tpl',
      1 => 1363553250,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21008130175144b8eb8008a2-00724733',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5144b8eb8e0515_80833487',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5144b8eb8e0515_80833487')) {function content_5144b8eb8e0515_80833487($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <title>Replicator | Team Re-Factor</title>
        
            <meta content="Richard Brooks" name="author">
            <meta content="Lebowski ipsum where&#8217;s my goddamn money, you bum?! Dolor sit amet, consectetur adipiscing elit praesent ac magna justo pellentesque ac. I mean &hellip;" name="description">
            <!-- http://t.co/dKP3o1e -->
            <meta content="True" name="HandheldFriendly">
            <meta content="320" name="MobileOptimized">
            <meta content="width=device-width, initial-scale=1" name="viewport">
            <link href="stylesheets/screen.css" media="screen, projection" rel="stylesheet" type="text/css"><!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
            <!--<link href="stylesheets/bootstrap.css" media="screen, projection" rel="stylesheet" type="text/css">-->
            <!--<link href="stylesheets/flat-ui.css" media="screen, projection" rel="stylesheet" type="text/css">-->
            <!--<link href="stylesheets/override.css" media="screen, projection" rel="stylesheet" type="text/css">-->
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
            <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700' rel='stylesheet' type='text/css'>
            <!--Fonts from Google"s Web font directory at http://google.com/webfonts -->
            <link href="http://fonts.googleapis.com/css?family=PT+Serif:regular,italic,bold,bolditalic" rel="stylesheet" type="text/css">
            <link href="http://fonts.googleapis.com/css?family=PT+Sans:regular,italic,bold,bolditalic" rel="stylesheet" type="text/css">
            <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
            <link href='http://fonts.googleapis.com/css?family=VT323' rel='stylesheet' type='text/css'>
            <link rel="icon" type="image/png" href="favicon.png">
        
    </head>
    <body>
        <div class="container">
            <div class="left-col">
                <div class="intrude-less">
                    <header class="inner" id="header">
                        <h1><a href="/wow">Replicator</a></h1>
                        <p class="subtitle">Team Re-Factor</p>
                        <nav id="main-nav">
                            <ul class="main-navigation">
                                <?php if (!isset($_SESSION['UserID'])){?>
                                <li><a href="login.php">Login</a></li>
                                <?php }?>
                                
                                <?php if (isset($_SESSION['UserID'])){?>
                                <li><a href="search.php">Search</a></li>
                                <li><a href="inventory.php">Items</a></li>
                                <?php }?>
                                <?php if (isset($_SESSION['UserID'])){?>
                                <li><a href="vendors.php">Vendors</a></li>
                                <li><a href="setLocation.php">Locate</a></li>
                                <li><a href="logout.php">Logout</a></li>
                                <?php }?>
                            </ul>
                        </nav>
                        <nav id="sub-nav"></nav>
                    </header>
                </div>
            </div>
            <div class="mid-col">
                <div class="mid-col-container">
                    <div class="inner" id="content">
                        <article class="post">
                            <h1 class="title">Edit Vendor</h1>
                            <div class="entry-content">
                                
<?php if ($_SESSION['isTechnician']){?>
    <form action="api.php" method="get">
        <input type="hidden" name="method" value="editVendor">
        <input type="hidden" name="vendorID" value="<?php echo $_smarty_tpl->tpl_vars['vendorID']->value;?>
">
        Vendor Name: <br /><input type="text" name="vendorname" value="<?php echo $_smarty_tpl->tpl_vars['vendor']->value['VendorName'];?>
" required><br>
        Latitude: <br /><input type="number" step=".000000001" name="latitude" value="<?php echo $_smarty_tpl->tpl_vars['vendor']->value['Latitude'];?>
" required><br>
        Longitude: <br /><input type="number" step=".000000001" name="longitude" value="<?php echo $_smarty_tpl->tpl_vars['vendor']->value['Longitude'];?>
" required><br>
        Address: <br /><input type="text" name="address" value="<?php echo $_smarty_tpl->tpl_vars['vendor']->value['Address'];?>
" required><br>
        Phone: <br /><input type="number" name="phone" value="<?php echo $_smarty_tpl->tpl_vars['vendor']->value['Phone'];?>
" required><br>
        EMail: <br /><input type="email" name="email" value="<?php echo $_smarty_tpl->tpl_vars['vendor']->value['EMail'];?>
" required><br>
        <input class="btn" type="submit" value="Save">
    </form>
    <?php if ($_SESSION['UserID']===$_smarty_tpl->tpl_vars['vendor']->value['Owner']){?>
    <input type="submit" value="Disown" class="btn btn-danger" onclick="location.href= 'api.php?method=disownVendor&vendorID=<?php echo $_smarty_tpl->tpl_vars['vendor']->value['VendorID'];?>
'; return false;"></input>
    <?php }elseif($_smarty_tpl->tpl_vars['vendor']->value['Owner']<=0){?>
    <input type="submit" value="Claim" class="btn btn-success" onclick="location.href= 'api.php?method=claimVendor&vendorID=<?php echo $_smarty_tpl->tpl_vars['vendor']->value['VendorID'];?>
'; return false;"></input>
    <?php }?>
<?php }else{ ?>
    You need to be logged in as a technician.
<?php }?>

                            </div>
                        </article>
                    </div>
                </div>
                <footer class="inner" id="footer">
                    <p>
                        &copy; 2013 Richard Brooks |
                        <span class="credit">Based on
                            <a href="http://shashankmehta.in/archive/2012/greyshade.html">Greyshade</a></span>
                    </p>
                </footer>
            </div>
        </div>
    </body>
</html><?php }} ?>