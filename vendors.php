<?php
require_once('./smarty/smarty_main.inc');
require_once('./methods.php');

$db = getDB();

$allVendors = getAllVendors();
$allVendors = $allVendors['result'];
$smarty->assign('vendors', $allVendors);

$smarty->display('extends:layout.tpl|vendors.tpl');

closeDB($db);
?>