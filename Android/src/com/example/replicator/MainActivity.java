package com.example.replicator;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.slidingmenu.lib.SlidingMenu;

public class MainActivity extends SherlockActivity {
	
	public static String getAllItems = "http://yare.us/wow/api.php?method=getAllItems";
	public static String currentJson = null;
	public static InputStream is = null;
	public static Item[] myItems = null;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        SlidingMenu menu = new SlidingMenu(getApplicationContext());
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setBehindOffset(500);
    	
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
        //gets all the items and sets them in myItems
        httpGet("http://yare.us/wow/api.php?method=getAllItems");   
    }
    
    public void continueUI() {
    	myItems = getItems(currentJson);
        for(int i = 0; i < myItems.length; i++) {
        	Log.i("#tardif", myItems[i].toString() );
        }
    	
    	//initialize display with item0
        Item currentItem = myItems[0];
        TextView tv = (TextView) findViewById(R.id.itemName);
        tv.setText(currentItem.getName() );
        tv = (TextView) findViewById(R.id.itemVendor);
        tv.setText(currentItem.getVendorName() );
        tv = (TextView) findViewById(R.id.itemDescription);
        tv.setText(currentItem.getDescription() );
        tv = (TextView) findViewById(R.id.itemScenario);
        tv.setText(currentItem.getScenario() );
        tv = (TextView) findViewById(R.id.itemClass);
        tv.setText(currentItem.getItemClass() );
        tv = (TextView) findViewById(R.id.itemCost);
        tv.setText(currentItem.getCost() );
    }
    
    private void httpGet(final String inputUrl){
    	new Thread(new Runnable() {
            public void run() {
            	currentJson = null;
            	HttpURLConnection connection = null;
                try {
					URL url = new URL(inputUrl);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.connect();
                    is = connection.getInputStream();
                    
                    currentJson = getStringFromInputStream(is);
                    continueUI();
                  
                } catch (MalformedURLException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                } finally {
                    if(null != connection) { connection.disconnect(); }
                }	
            }
        }).start();
    }
    
    private String getStringFromInputStream(InputStream is){
    	String result = "";
    	try{
    		int c = -1;
    		while((c = is.read()) != -1){
    			char ch = (char) c;
    			result = result + ch;
    		}
    	}catch(Exception e){
    		return null;
    	}
    	return result;
    }
    
    private Item[] getItems(String jsonString){
    	
    	/*JSONObject obj = null;
		try {
			obj = new JSONObject(json);
			return obj.getString("name");
		}
		catch(JSONException e) {
			return null;
		} */
    	String name;
    	int cost;
    	int itemId;
    	String picture;
    	String vendorName;
    	String description;
    	String scenario;
    	String itemClass;
    	Item[] createdItems = null; 
    	try {
    		JSONObject json = new JSONObject(jsonString);
    		JSONArray jArray = json.getJSONArray("result");
    		//Log.i("#tardif", jArray.toString() );

    		createdItems = new Item[jArray.length()];
    		for(int i=0;i<jArray.length();i++){
    			JSONObject json_data = jArray.getJSONObject(i);
    			Log.i("#tardif", json_data.toString() );
    			name = json_data.getString("Name");
    			cost = json_data.getInt("Cost");
    			itemId = json_data.getInt("ItemID");
    			picture = json_data.getString("Picture");
    			vendorName = json_data.getString("VendorName");
    			description = json_data.getString("Description");
    			scenario = json_data.getString("Scenario");
    			itemClass = json_data.getString("Class");
    			createdItems[i] = new Item(name, picture, description, vendorName, scenario, itemClass, cost, itemId);
    		}
    	}
    	catch(JSONException e) {
    		
    	}
    	return createdItems;
    	 /*Log.i("log_tag","_id"+json_data.getInt("account")+
    	  ", mall_name"+json_data.getString("name")+
    	  ", location"+json_data.getString("number")+
    	  ", telephone"+json_data.getString("url")+
    	  ",----"+json_data.getString("balance")+
    	  ",----"+json_data.getString("credit")+
    	  ",----"+json_data.getString("displayName")
    	 );
    	 } */
    }
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       MenuInflater inflater = getSupportMenuInflater();
       inflater.inflate(R.menu.main, menu);
       return true;
    }

    
}