{extends file='layout.tpl'}

{block name=pageTitle}Edit Item{/block}

{block name=pageContent}
<input value="Add Image" type="submit" class='btn' onclick="{literal}filepicker.pickAndStore({mimetype:'image/*'},
        {location:'S3'}, function(fpfiles){
   console.log(fpfiles);
   window.location.href = 'http://yare.us/wow/api.php?method=addItemImage&itemID={/literal}{$itemID}{literal}&itemImage=' + fpfiles[0].url.substr(fpfiles[0].url.lastIndexOf('/')+1, fpfiles[0].url.length) + '&fromTechnician={/literal}{if $smarty.session.isTechnician == true}true{else}false{/if}{literal}';
});{/literal}"></input><br />
{if $smarty.session.isTechnician}
    <form action="api.php" method="get">
        <input type="hidden" name="method" value="editItem">
        <input type="hidden" name="itemID" value="{$itemID}">
        <strong>Name:</strong> <br /><input type="text" name="name" value="{$item['Name']}" required><br>
        <strong>Description:</strong> <br /><textarea style="height: 5em; width:250px" type="text" name="description" required>{$item['Description']}</textarea><br>
        <strong>Vendor Name:</strong> <br /><input type="text" name="vendorname" value="{$item['VendorName']}" required><br>
        <strong>Scenario:</strong> <br /><input type="text" name="scenario" value="{$item['Scenario']}" list="scenarios" required><br>
        <datalist id="scenarios">
           <option value="Morning">
           <option value="Day">
           <option value="Night">
        </datalist>
        <strong>Class:</strong> <br /><input type="text" name="class" value="{$item['Class']}" list="itemClasses" required><br>
        <datalist id="itemClasses">
           <option value="SMG">
           <option value="Pistol">
           <option value="Shotgun">
           <option value="Eridian">
           <option value="Drink">
           <option value="Sniper Rifle">
           <option value="Shield">
        </datalist>
        <strong>Cost:</strong> <br /><input type="number" name="cost" value="{$item['Cost']}" required><br>
        <input type="submit" value="Save" class="btn"><br />
        <strong>Attributes:</strong>
        
    </form>
{else}
    <strong>Name:</strong> <br />{$item['Name']} <br />
    <strong>Description:</strong> <br />{$item['Description']}<br />
    <strong>Vendor Name:</strong> <br />{$item['VendorName']}<br />
    <strong>Scenario:</strong> <br />{$item['Scenario']}<br />
    <strong>Class:</strong> <br />{$item['Class']}<br />
    <strong>Cost:</strong> <br />{$item['Cost']}<br />
    <strong>Attrbutes:</strong><br />
{/if}
<ul style="list-style-type: none; margin-left: 0">
{foreach from=$itemAttributes item=attribute}
    <li><input style="width:55px; margin-right:10px" type="submit" value="{$attribute['Score']}" class="btn btn-large disabled"><strong style="width: 5em; display: inline-block; overflow:hidden; text-overflow:ellipsis;">{$attribute['AttributeName']}</strong><input style="width:35px; margin-right:0" type="submit" value="-1" class="btn" onclick="location.href= 'api.php?method=downVoteAttribute&attributeID={$attribute['AttributeID']}'; return false;"><input style="width:35px; margin-right:0" type="submit" value="+1" class="btn" onclick="location.href= 'api.php?method=upVoteAttribute&attributeID={$attribute['AttributeID']}'; return false;">{if $attribute['Score'] lte 0}<input style="width:35px" type="submit" value="X" class="btn btn-danger" onclick="location.href= 'api.php?method=deleteAttribute&attributeID={$attribute['AttributeID']}'; return false;">{/if}</li>
{/foreach}
</ul>
<form action="api.php" method="get">
<input type="hidden" name="method" value="addAttribute">
<input type="hidden" name="itemID" value="{$itemID}">
<input type="text" name="attributename" list="attributeNames">
<datalist id="attributeNames">
    <option value="Poison">
    <option value="Fire">
    <option value="Explosion">
    <option value="Eridian">
    <option value="Corrosive">
    <option value="Shock">
    <option value="Slag">
</datalist>
<br />
<input type="submit" value="New Attribute" class="btn">

<br />

{literal}
<script type="text/javascript">
function loadScript() {
    filepicker.setKey('AzGFiOmvMSR2sEYoBUJulz');
}

window.onload = loadScript;

(function(a){if(window.filepicker){return}var b=a.createElement("script");b.type="text/javascript";b.async=!0;b.src=("https:"===a.location.protocol?"https:":"http:")+"//api.filepicker.io/v1/filepicker.js";var c=a.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c);var d={};d._queue=[];var e="pick,pickMultiple,pickAndStore,read,write,writeUrl,export,convert,store,storeUrl,remove,stat,setKey,constructWidget,makeDropPane".split(",");var f=function(a,b){return function(){b.push([a,arguments])}};for(var g=0;g<e.length;g++){d[e[g]]=f(e[g],d._queue)}window.filepicker=d})(document); 
</script>
{/literal}
{/block}