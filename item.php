<?php

require_once('./smarty/smarty_main.inc');
require_once('./methods.php');

$db = getDB();

if (isset($_GET['itemID'])) {
    $itemID = $_GET['itemID'];
    $item = getItemByID($itemID);
    $item = $item['result'][0];

    $itemAttributes = getAttributesByItemID($itemID);
    if (!isset($itemAttributes['error'])) {
        $itemAttributes = $itemAttributes['result'];
    } else {
        $itemAttributes = array();
    }
    

    $itemImages = getImagesByItemID($itemID);
    if (!isset($itemImages['error'])) {
        $itemImages = $itemImages['result'];
    } else {
        $itemImages = array();
    }

    $smarty->assign('item', $item);
    $smarty->assign('itemAttributes', $itemAttributes);
    $smarty->assign('itemImages', $itemImages);

    $smarty->display('extends:layout.tpl|item.tpl');
}

closeDB($db);

?>