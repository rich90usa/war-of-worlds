{extends file='layout.tpl'}

{block name=pageTitle}Register{/block}

{block name=pageContent}
<form action="api.php?method=register" method="post">
    Username: <br /><input type="text" name="username" required><br>
    Password: <br /><input type="password" name="password" required><br>
    <label for="isTechnician">Technician:</label> <input style="padding: 0; margin: 0; vertical-align: bottom; position: relative; top: 1px;" class="toggle toggle-icon toggle-radio" type="checkbox" name="isTechnician"><br>
    <input class="btn" type="submit" value="Submit">
</form>
{/block}