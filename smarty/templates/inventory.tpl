{extends file='layout.tpl'}

{block name=pageTitle}Items{/block}

{block name=pageContent}
{if $smarty.session.isTechnician}<input type="submit" value="Create Item" class="btn" onclick="location.href= 'addItem.php'; return false;"></input>{/if}
<ul style="list-style-type: none; margin-left: 0">
{foreach from=$inventory item=item}
    <li class="btn disabled" style="background-color: whitesmoke; padding:0;"><input style="width:220px" type="submit" value="{$item['Name']}" class="btn btn-large" onclick="location.href= 'item.php?itemID={$item['ItemID']}'; return false;"></input><input style="width:100px" type="submit" value="Edit" class="btn" onclick="location.href= 'editItem.php?itemID={$item['ItemID']}'; return false;"></input>{if $smarty.session.isTechnician}<input style="width:100px" type="submit" value="Delete" class="btn btn-danger" onclick="location.href= 'api.php?method=deleteItem&itemID={$item['ItemID']}'; return false;">{/if}</li>
{/foreach}
</ul>
{/block}