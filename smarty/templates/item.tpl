{extends file='layout.tpl'}

{block name=pageTitle}View Item{/block}

{block name=pageContent}
<div id="map-canvas" style="height:300px"></div><br />
<input type="submit" value="Edit" class="btn" onclick="location.href= 'editItem.php?itemID={$item['ItemID']}'; return false;"></input><input type="submit" value="Directions" class="btn btn-info" onclick="location.href= 'https://maps.google.com/maps?saddr={$smarty.session.Latitude},+{$smarty.session.Longitude}&daddr={$item['Latitude']},+{$item['Longitude']}'; return false;"></input><br /><br />
{if count($itemImages) gt 0}
{foreach $itemImages item=itemImage}
    <img src="http://www.filepicker.io/api/file/{$itemImage['ImageURL']}" data-fromTechnician="{$itemImage['fromTechnician']}" /><br />
{/foreach}
{/if}
<strong>Name:</strong><br /> {$item['Name']} <br />
<strong>Description:</strong><br /> {$item['Description']}<br />
<strong>Vendor Name:</strong><br /> {$item['VendorName']}<br />
<strong>Scenario:</strong><br /> {$item['Scenario']}<br />
<strong>Class:</strong><br /> {$item['Class']}<br />
<strong>Cost:</strong><br /> {$item['Cost']}<br />
{if $itemAttributes}
    <strong>Attrbutes:</strong><br />
    <ul style="list-style-type: none; margin-left: 0">
    {foreach from=$itemAttributes item=attribute}
        <li><input style="width:55px; margin-right:10px" type="submit" value="{$attribute['Score']}" class="btn btn-large disabled"><strong style="width: 5em; display: inline-block; overflow:hidden; text-overflow:ellipsis;">{$attribute['AttributeName']}</strong></li>
    {/foreach}
    </ul>
{/if}
<script type="text/javascript">
function initialize() {
  var mapOptions = {
    zoom: 13,
    center: new google.maps.LatLng({$smarty.session.Latitude}, {$smarty.session.Longitude}),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
    {if strlen($item['Latitude']) gt 0 and strlen($item['Longitude']) gt 0}
    var marker = 
        new google.maps.Marker({
            map:map,
            icon: {
              path: google.maps.SymbolPath.CIRCLE,
              fillColor: 'white',
              strokeColor: 'red',
              fillOpacity: 1.0,
              strokeOpacity: 1.0,
              strokeWeight: 4.0,
              scale: 5
            },
            draggable:false,
            //animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng({$item['Latitude']}, {$item['Longitude']})
        });
    {/if}
    var userMarker = 
        new google.maps.Marker({
            map:map,
            icon: {
              path: google.maps.SymbolPath.CIRCLE,
              fillColor: 'red',
              strokeColor: 'white',
              fillOpacity: 1.0,
              strokeOpacity: 1.0,
              strokeWeight: 4.0,
              scale: 5
            },
            draggable:false,
            //animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng({$smarty.session.Latitude}, {$smarty.session.Longitude})
        });
}

function loadScript() {
  var script = document.createElement("script");
  script.type = "text/javascript";
  script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyCv2K7qIWGVpygfRaf9rqspLFR-fSfnlzU&sensor=true&callback=initialize";
  document.body.appendChild(script);
}


window.onload = loadScript;
</script>
{/block}