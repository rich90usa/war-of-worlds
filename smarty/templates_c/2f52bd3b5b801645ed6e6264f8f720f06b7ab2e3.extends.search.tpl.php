<?php /* Smarty version Smarty-3.1.13, created on 2013-03-18 01:20:20
         compiled from "/var/www/sites/yare/wow/smarty/templates/search.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20140789525144c8607b1951-62432280%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2f52bd3b5b801645ed6e6264f8f720f06b7ab2e3' => 
    array (
      0 => '/var/www/sites/yare/wow/smarty/templates/search.tpl',
      1 => 1363555220,
      2 => 'file',
    ),
    'fc05a559fcc48881c073fded6a8797d44a7c3a5f' => 
    array (
      0 => '/var/www/sites/yare/wow/smarty/templates/layout.tpl',
      1 => 1363554592,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20140789525144c8607b1951-62432280',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5144c86089c4b6_48420489',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5144c86089c4b6_48420489')) {function content_5144c86089c4b6_48420489($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <title>Replicator | Team Re-Factor</title>
        
            <meta content="Richard Brooks" name="author">
            <meta content="Lebowski ipsum where&#8217;s my goddamn money, you bum?! Dolor sit amet, consectetur adipiscing elit praesent ac magna justo pellentesque ac. I mean &hellip;" name="description">
            <!-- http://t.co/dKP3o1e -->
            <meta content="True" name="HandheldFriendly">
            <meta content="320" name="MobileOptimized">
            <meta content="width=device-width, initial-scale=1" name="viewport">
            <link href="stylesheets/screen.css" media="screen, projection" rel="stylesheet" type="text/css"><!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
            <!--<link href="stylesheets/bootstrap.css" media="screen, projection" rel="stylesheet" type="text/css">-->
            <!--<link href="stylesheets/flat-ui.css" media="screen, projection" rel="stylesheet" type="text/css">-->
            <!--<link href="stylesheets/override.css" media="screen, projection" rel="stylesheet" type="text/css">-->
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
            <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700' rel='stylesheet' type='text/css'>
            <!--Fonts from Google"s Web font directory at http://google.com/webfonts -->
            <link href="http://fonts.googleapis.com/css?family=PT+Serif:regular,italic,bold,bolditalic" rel="stylesheet" type="text/css">
            <link href="http://fonts.googleapis.com/css?family=PT+Sans:regular,italic,bold,bolditalic" rel="stylesheet" type="text/css">
            <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
            <link href='http://fonts.googleapis.com/css?family=VT323' rel='stylesheet' type='text/css'>
            <link rel="icon" type="image/png" href="favicon.png">
        
    </head>
    <body>
        <div class="container">
            <div class="left-col">
                <div class="intrude-less">
                    <header class="inner" id="header">
                        <a href="/wow"><img src="logo.png"></a>
                        <div style="display:none">
                            <h1><a href="/wow">Replicator</a></h1>
                            <p class="subtitle">Team Re-Factor</p>
                        </div>
                        <nav id="main-nav">
                            <ul class="main-navigation">
                                <?php if (!isset($_SESSION['UserID'])){?>
                                <li><a href="login.php">Login</a></li>
                                <?php }?>
                                
                                <?php if (isset($_SESSION['UserID'])){?>
                                <li><a href="search.php">Search</a></li>
                                <li><a href="inventory.php">Items</a></li>
                                <?php }?>
                                <?php if (isset($_SESSION['UserID'])){?>
                                <li><a href="vendors.php">Vendors</a></li>
                                <li><a href="setLocation.php">Locate</a></li>
                                <li><a href="logout.php">Logout</a></li>
                                <?php }?>
                            </ul>
                        </nav>
                        <nav id="sub-nav"></nav>
                    </header>
                </div>
            </div>
            <div class="mid-col">
                <div class="mid-col-container">
                    <div class="inner" id="content">
                        <article class="post">
                            <h1 class="title">Search Items</h1>
                            <div class="entry-content">
                                
<form action="search.php" method="get">
    <input type="search" name="name" list="searchSuggest"><br />
    <datalist id="searchSuggest">
        <option value="Day">
        <option value="Morning">
        <option value="Night">
        <option value="Dahl">
        <option value="Tediore">
        <option value="Torgue">
        <option value="Maliwan">
        <option value="Hyperion">
        <option value="Launcher">
        <option value="SMG">
        <option value="Assault Rifle">
        <option value="Pistol">
        <option value="Shotgun">
        <option value="Sniper Rifle">
        <option value="Atlas">
        <option value="Jakobs">
        <option value="Vladof">
        <option value="Eridian">
    </datalist>
</form>
<?php if ($_smarty_tpl->tpl_vars['searchResults']->value){?>
    <strong>Results for "</strong><?php echo $_smarty_tpl->tpl_vars['searchQuery']->value;?>
<strong>":</strong><br />
    <ul style="list-style-type: none; margin-left: 0">
    <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['searchResults']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
        <li class="btn disabled" style="background-color: whitesmoke; padding:0;"><input style="width:220px" type="submit" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['Name'];?>
" class="btn btn-large" onclick="location.href= 'item.php?itemID=<?php echo $_smarty_tpl->tpl_vars['item']->value['ItemID'];?>
'; return false;"></input><input style="width:100px" type="submit" value="Edit" class="btn" onclick="location.href= 'editItem.php?itemID=<?php echo $_smarty_tpl->tpl_vars['item']->value['ItemID'];?>
'; return false;"></input><?php if ($_SESSION['isTechnician']){?><input style="width:100px" type="submit" value="Delete" class="btn btn-danger" onclick="location.href= 'api.php?method=deleteItem&itemID=<?php echo $_smarty_tpl->tpl_vars['item']->value['ItemID'];?>
'; return false;"><?php }?></li>
    <?php } ?>
    </ul>
<?php }?>

                            </div>
                        </article>
                    </div>
                </div>
                <footer class="inner" id="footer">
                    <p>
                        &copy; 2013 Richard Brooks |
                        <span class="credit">Based on
                            <a href="http://shashankmehta.in/archive/2012/greyshade.html">Greyshade</a></span>
                    </p>
                </footer>
            </div>
        </div>
    </body>
</html><?php }} ?>