<?php

require_once('./smarty/smarty_main.inc');
require_once('./methods.php');

$db = getDB();

if (isset($_GET['itemID'])) {
    $itemID = $_GET['itemID'];

    $item = getItemByID($itemID);
    $item = $item['result'][0];

    $itemAttributes = getAttributesByItemID($itemID);
    $itemAttributes = $itemAttributes['result'];

    $smarty->assign('item', $item);
    $smarty->assign('itemID', $itemID);
    $smarty->assign('itemAttributes', $itemAttributes);
}

$smarty->display('extends:layout.tpl|editItem.tpl');

closeDB($db);

?>