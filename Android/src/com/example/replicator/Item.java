package com.example.replicator;

public class Item {
	private String name;
	private String picture;
	private String description;
	private String vendorName;
	private String scenario;
	private String itemClass;
	private int cost;
	private int itemId;
	// attributes?

	public Item(String n, String p, String d, String v, String s, String ic, int c, int iId) {
		// TODO Auto-generated constructor stub
		this.name = n;
		this.picture = p;
		this.description = d;
		this.vendorName = v;
		this.scenario = s;
		this.itemClass = ic;
		this.cost = c;
		this.itemId = iId;
	}
	
	@Override
	public String toString() {
		return "Item [name=" + name + ", picture=" + picture + ", description="
				+ description + ", vendorName=" + vendorName + ", scenario="
				+ scenario + ", itemClass=" + itemClass + ", cost=" + cost
				+ ", itemId=" + itemId + "]";
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return picture;
	}

	public void setImage(String image) {
		this.picture = image;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getScenario() {
		return scenario;
	}

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	public String getItemClass() {
		return itemClass;
	}

	public void setItemClass(String itemClass) {
		this.itemClass = itemClass;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

}
